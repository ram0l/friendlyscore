<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transactions
 *
 * @ORM\Table(name="transactions", uniqueConstraints={@ORM\UniqueConstraint(name="transactions_transactionid_uindex", columns={"transaction_id"})})
 * @ORM\Entity
 */
class Transactions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="transactions_id_seq", allocationSize=1, initialValue=1)
     */
    private int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_id", type="string", length=50, nullable=false)
     */
    private string $transactionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="transaction_information", type="string", length=250, nullable=true)
     */
    private ?string $transactionInformation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="amount", type="decimal", precision=8, scale=2, nullable=true)
     */
    private ?string $amount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="currency", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private ?string $currency;

    /**
     * @var string|null
     *
     * @ORM\Column(name="category", type="string", length=25, nullable=true)
     */
    private ?string $category;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="value_date_time", type="datetime", nullable=true)
     */
    private ?\DateTime $valueDateTime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    public function setTransactionId(string $transactionId): self
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    public function getTransactionInformation(): ?string
    {
        return $this->transactionInformation;
    }

    public function setTransactionInformation(?string $transactionInformation): self
    {
        $this->transactionInformation = $transactionInformation;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(?string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getValueDateTime(): ?\DateTimeInterface
    {
        return $this->valueDateTime;
    }

    public function setValueDateTime(?\DateTimeInterface $valueDateTime): self
    {
        $this->valueDateTime = $valueDateTime;

        return $this;
    }


}
