<?php

namespace App\Repository;

use App\Entity\Transactions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * @method Transactions|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transactions|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transactions[]    findAll()
 * @method Transactions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends ServiceEntityRepository
{
    /**
     * TransactionRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transactions::class);
    }

    /**
     * @param array $transactionList
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\Persistence\Mapping\MappingException
     */
    public function addTransactions(array $transactionList)
    {
        $categories = [
            'Food', 'Health', 'Transport', 'Other'
        ];

        /** @var EntityManager $em */
        $em = $this->getEntityManager();
        $batchSize = 20;

        foreach ($transactionList as $k => $v) {
            $randomCategory = $categories[rand(0, count($categories) - 1)];

            $transaction = new Transactions();
            $transaction->setTransactionid($v['TransactionId']);
            $transaction->setValuedatetime(new \DateTime($v['ValueDateTime']));
            $transaction->setAmount($v['Amount']['Amount']);
            $transaction->setCurrency($v['Amount']['Currency']);
            $transaction->setCategory($randomCategory);
            $transaction->setTransactioninformation($v['TransactionInformation']);

            $em->persist($transaction);
            if (($k % $batchSize) === 0) {
                $em->flush();
                $em->clear(); // Detaches all objects from Doctrine!
            }
        }

        $em->flush(); //Persist objects that did not make up an entire batch
        $em->clear();
    }

    /**
     * @return \DateTime
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLastTransactionDate(): \DateTime
    {

        try {
            $em = $this->getEntityManager();
            $q = $em->createQueryBuilder()
                ->select('t.valueDateTime')
                ->from(Transactions::class, 't')
                ->orderBy('t.valueDateTime', 'desc')
                ->setMaxResults(1)
                ->getQuery();
            $result = $q->getResult();
        } catch (NoResultException $e) {
            throw new Exception($e->getMessage());
        } catch (NonUniqueResultException $e) {
            throw new Exception($e->getMessage());
        }

        if (count($result) === 0) {
            return new \DateTime(date('Y-m-d H:i:s', time() - (86400 * 365)));
        }

        return $result[0]['valueDateTime'];
    }

    /**
     * @param string $id
     * @return Transactions
     */
    public function findByTransactionId(string $id): ?Transactions
    {
        return $this->findOneBy(['transactionId' => $id]);
    }

    /**
     * @param Transactions $entity
     * @param string $category
     * @return bool
     */
    public function updateCategoryForSimilarRecords(Transactions $entity, string $category): bool
    {
        $information = $entity->getTransactioninformation();
        $information = substr($information, 0, strpos($information, ' ON '));
        $em = $this->getEntityManager();
        $q = $em->createQueryBuilder('t')
            ->update(Transactions::class, 't')
            ->where('t.transactionInformation like :info')
            ->set('t.category', ':category')
            ->setParameter('info', $information . '%')
            ->setParameter('category', $category)
            ->getQuery();

        return (bool)$q->execute();

    }
}
