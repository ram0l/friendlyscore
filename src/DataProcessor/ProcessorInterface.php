<?php

namespace App\DataProcessor;

interface ProcessorInterface
{
    public function getRecent();
}