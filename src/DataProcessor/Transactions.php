<?php

namespace App\DataProcessor;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Transactions implements ProcessorInterface
{
    private HttpClientInterface $client;
    private LoggerInterface $logger;
    private ParameterBagInterface $bag;

    /**
     * Transactions constructor.
     *
     * @param HttpClientInterface $client
     * @param LoggerInterface $logger
     * @param ParameterBagInterface $bag
     */
    public function __construct(HttpClientInterface $client, LoggerInterface $logger, ParameterBagInterface $bag)
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->bag = $bag;
    }

    /**
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     */
    public function getRecent(): array
    {
        try {
            $data = $this->client->request('GET', $this->bag->get('fs_endpoint'))->toArray();
            if (isset($data['Data']['Transaction'])) {
                return $data['Data']['Transaction'];
            }

            return [];
        } catch (RedirectionExceptionInterface | TransportExceptionInterface | ServerExceptionInterface $e) {
            $this->logger->debug('FriendlyScore API down?');
            $this->logger->error('Error while retrieving data');
            throw new \Exception('Error while running command', 0, $e);
        } catch (DecodingExceptionInterface $e) {
            $this->logger->debug('Malformed data');
            $this->logger->error('Malformed data');
            throw new \Exception('Error while running command', 0, $e);
        } catch (\Exception $e) {
            $this->logger->error('Boooom!');
            throw new \Exception('Error while running command', 0, $e);
        }
    }

    /**
     * @param array $transactionList
     * @param \DateTime $lastTransactionDate
     * @return array
     * @throws \Exception
     */
    public function removeOlderThan(array $transactionList, \DateTime $lastTransactionDate): array
    {
        $newList = [];
        foreach ($transactionList as $k => $v) {
            $elementDate = new \DateTime($v['ValueDateTime']);
            if ($elementDate > $lastTransactionDate) {
                $newList[] = $v;
            }
        }

        return $newList;
    }
}