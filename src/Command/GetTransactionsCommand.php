<?php

namespace App\Command;

use App\DataProcessor\Transactions;
use App\Repository\TransactionRepository;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetTransactionsCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:get-transactions';

    /** @var Transactions */
    private Transactions $processor;

    /** @var TransactionRepository */
    private TransactionRepository $transactionRepository;

    /** @var LoggerInterface */
    private LoggerInterface $logger;

    public function __construct(Transactions $processor, TransactionRepository $repository, LoggerInterface $logger)
    {
        $this->processor = $processor;
        $this->transactionRepository = $repository;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Retrieves transactions from remote server');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $transactionList = $this->processor->getRecent();
            $lastTransactionDate = $this->transactionRepository->getLastTransactionDate();
            $transactionList = $this->processor->removeOlderThan($transactionList, $lastTransactionDate);
            $this->transactionRepository->addTransactions($transactionList);
        } catch (\RuntimeException $e) {
            $this->logger->error($e->getMessage());
            return Command::FAILURE;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return Command::FAILURE;
        } catch (ClientExceptionInterface $e) {
            $this->logger->error($e->getMessage());
            return Command::FAILURE;
        }

        $output->writeln('Success :)');
        return Command::SUCCESS;
    }
}
