<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use App\Repository\TransactionRepository;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TransactionsController extends AbstractController
{
    /**
     * @param TransactionRepository $transactionRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function getAll(TransactionRepository $transactionRepository, SerializerInterface $serializer): JsonResponse
    {
        try {
            $data = $transactionRepository->findAll();
            return $this->json([
                'data' => [
                    'transactions' => $serializer->normalize($data)
                ]
            ]);
        } catch (\Exception $e) {
            return $this->json(['message' => $e->getMessage()]);
        }
    }

    /**
     * @param Request $request
     * @param TransactionRepository $transactionRepository
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function update(Request $request,
                           TransactionRepository $transactionRepository,
                           ValidatorInterface $validator): JsonResponse
    {
        $transactionId = $request->get('id');
        $category = $request->get('category');

        /*
         * validation of input data
         */
        $errors = $validator->validate($category, [
            new NotBlank(['message' => 'Category property not found']),
            new Choice([
                'choices' => $this->getParameter('allowed_categories'),
                'message' => 'Category is invalid'
            ])
        ]);
        if (count($errors)) {
            $msg = [];
            foreach ($errors as $error) {
                $msg[] = $error->getMessage();
            }
            return $this->json(['message' => $msg], 400);
        }

        /*
         * update
         */
        $transaction = $transactionRepository->findByTransactionId($transactionId);
        if (!$transaction) {
            return $this->json(['message' => 'Transaction not found'], 404);
            //throw $this->createNotFoundException('Transaction not found');
        }

        $transactionRepository->updateCategoryForSimilarRecords($transaction, $category);

        return $this->json(['status' => 'ok']);
    }
}
