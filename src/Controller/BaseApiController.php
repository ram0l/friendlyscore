<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class BaseApiController
{
    /**
     * @var JsonResponse
     */
    protected $response;

    /**
     * BaseApiController constructor.
     * @param Request $request
     */
    public function __construct()
    {
        $this->response = new JsonResponse();
    }

    /**
     * @param Request $request
     */
    public function convertJsonStringToArray(Request $request)
    {
        if ($request->getContentType() != 'json' || !$request->getContent()) {
            return;
        }

        $data = json_decode($request->getContent(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new BadRequestHttpException('invalid json body: ' . json_last_error_msg());
        }

        $request->request->replace(is_array($data) ? $data : array());
    }
}