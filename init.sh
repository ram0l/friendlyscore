#!/bin/bash

docker-compose up -d
sleep 10
docker exec -it php bin/console doctrine:database:import ./docker/db/table_1.sql
docker exec -it php bin/console doctrine:database:import ./docker/db/table_2.sql
docker exec -it php bin/console doctrine:database:import ./docker/db/table_3.sql