create table if not exists transactions
(
	id serial not null
		constraint transactions_pk
			primary key,
	transaction_id varchar(50) not null,
	transaction_information varchar(250),
	amount numeric(8,2),
	currency char(3),
	category varchar(25),
	value_date_time timestamp
);
