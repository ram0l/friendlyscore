Assignment specification
========================

The task is to build a simple backend application/microservice in PHP, using Symfony,
which has a simple API and acts as a client of an external API.

Requirements
------------

- Symfony framework;
- Propel 1.6 or Doctrine ORM;
- PostgreSQL Database;
- REST API with 2 endpoints (listed in the __Features and flow__ section).


How to run project
------------------

Clone this repository
`git clone https://gitlab.com/ram0l/friendlyscore.git`
Run docker-compose  
`chmod +x init.sh`  
`./init.sh`

CLI:
`docker exec -it php bin/console app:get-transaction`

API Endpoints:  
`GET 0.0.0.0/v1/transactions` - returns transaction list  
`PUT 0.0.0.0/v1/transactions/ID` - updates transaction where ID

Body:  
```json
{"category":"Services"}
```

